﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class simulation_class
    {
        public IEnumerable<int> Iter(int start, int end)
        {
            for (int i = start; i < end; i++)
            {
                yield return i;
            }
        }

        public void run()
        {
            foreach (int r in Iter(1,10))
            {
                Console.WriteLine($"{r}");
                Console.Read();
            }
        }

        public IEnumerable<double> Generate_X(int times)
        {
            Random rd = new Random();

            for (int i = 0; i < times; i++)
            {
                yield return rd.NextDouble();
            }
            
        }

        public IEnumerable<double> Generate_Y(int times)
        {
            Random rd = new Random();

            for (int i = 0; i < times; i++)
            {
                yield return rd.NextDouble();
            }

        }

    }
}
