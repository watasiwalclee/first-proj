﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;


namespace WindowsFormsApp1
{

    public partial class Form1 : Form
    {
        public void Initialized(object sender, EventArgs e)
        {
            chart1.Series.Add("scatter plot");  // 增加軸物件
            chart1.Series["scatter plot"].ChartType = SeriesChartType.Point;
            chart1.Series["scatter plot"].MarkerStyle = MarkerStyle.Diamond;
            chart1.Series["scatter plot"].MarkerColor = Color.CornflowerBlue;

            win_clear();
            panel1.Show();
        }

        public void win_clear()
        {
            panel1.Hide();
            panel2.Hide();
            panel3.Hide();
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }
        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            
        }
        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            win_clear();
            panel1.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            win_clear();
            panel2.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            win_clear();
            panel3.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            simulation_class sc = new simulation_class();
            sc.run();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            simulation_class sc = new simulation_class();
            
            int times = 30;

            foreach (float y in sc.Generate_Y(times))
            {
                chart1.Series["scatter plot"].Points.AddY(y);
            }
            

        }

        private void button5_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.ShowDialog();                  //開啟選取檔案視窗
            textBox1.Text = file.FileName;      //將選取的檔案名稱呈現在文字方塊上

            //this.txtFile.Text = file.SafeFileName;
        }


    }
}
